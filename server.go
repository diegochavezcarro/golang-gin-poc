package main

import (
	"os"

	"github.com/gin-gonic/gin"
	"gitlab.com/diegochavezcarro/golang-gin-poc/controller"
	"gitlab.com/diegochavezcarro/golang-gin-poc/service"
)

var (
	videoService    service.VideoService       = service.New()
	videoController controller.VideoController = controller.New(videoService)
)

func main() {
	server := gin.Default()
	server.GET("/hello", func(ctx *gin.Context) {
		ctx.String(200, "Hello!")
	})
	server.GET("/videos", func(ctx *gin.Context) {
		ctx.JSON(200, videoController.FindAll())
	})
	server.POST("/videos", func(ctx *gin.Context) {
		ctx.JSON(200, videoController.Save(ctx))
	})
	port := os.Getenv("PORT")
	// que la nube decida el puerto
	if port == "" {
		port = "5000"
	}
	server.Run(":" + port)

}
