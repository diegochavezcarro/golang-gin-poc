basado en (ver tmb sus otros branches):

https://gitlab.com/pragmaticreviews/golang-gin-poc/-/tree/ci-cd

go mod init gitlab.com/diegochavezcarro/golang-gin-poc

dsps de escribir parte del codigo:

go mod tidy

go get github.com/stretchr/testify

go get github.com/gin-gonic/gin

Despues de escribir codigo

go mod tidy

Configurar un Gitlab propio:

git init

git remote add origin https://gitlab.com/diegochavezcarro/golang-gin-poc.git

git add .

git commit -m "Primera subida"

Algunos prefieren llamarlo master, por lo gral se esta usando main:

git branch -M main

git push -u origin main

Despues crear el .gitlab-ci.yml

Darse de alta en heroku (https://dashboard.heroku.com/)

Crear una app con determinado nombre, por ej: 

dchavez-golang-gin-poc

En Profile->Account Settings obtener la API Key
abc07c4e-a289-402b-9e13-cfdab254593d

Dentro de Gitlab, entrar en Settings->CI/CD->Variables

Agregar el valor obtenido anteriormente en una nueva variable llamada HEROKU_API_KEY 

Para utilizar en gitlab runners compartidos agregar la tarjeta de credito, no va a cargar nada. Sino crear runners propios: https://docs.gitlab.com/runner/register/

Subir codigo (add, commit, push)

Si se quieren bajar el cliente de heroku para por ej, ver logs:

brew tap heroku/brew && brew install heroku

heroku login

heroku logs -a dchavez-golang-gin-poc --tail

Ver que se cambio el server.go para q no se asigne un puerto

A veces heroku no detecta donde arrancar la app, en ese caso agregar en el raiz un archivo "Procfile" (sin extension) con este contenido:
web: bin/golang-gin-poc



